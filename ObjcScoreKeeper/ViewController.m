//
//  ViewController.m
//  ObjcScoreKeeper
//
//  Created by moha on 11/6/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//


#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *team1ScoreLbl;
@property (strong, nonatomic) IBOutlet UIStepper *team1Stepper;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (IBAction)team1StepperChanged:(UIStepper *)sender {
    self.team1ScoreLbl.text = [NSString stringWithFormat:@"%.f", sender.value];
}

- (IBAction)resetBtnTouched:(UIButton *)sender {
    self.team1ScoreLbl.text = @"0";
    self.team1Stepper.value = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
