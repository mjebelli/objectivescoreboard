//
//  main.m
//  ObjcScoreKeeper
//
//  Created by moha on 11/6/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
