//
//  AppDelegate.h
//  ObjcScoreKeeper
//
//  Created by moha on 11/6/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

